using PdUtils.MailService;
using PdUtils.MailService.Impl;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace PdUtilsSamples
{
    public class MailForm : MonoBehaviour
    {
        public InputField InputEmail;
        public InputField InputSubject;
        public InputField InputBody;
        
        public Button ButtonOpenMail;
        
        private readonly MailService _mailService = new MailService();
        
        private void Awake()
        {
            ButtonOpenMail.OnClickAsObservable().Subscribe(_ => OpenMail()).AddTo(ButtonOpenMail);
        }

        private void OpenMail()
        {
            var body = "Hello, Playdarium \n \n \n "
                       + "\n Best regards, "
                       + "\n"
                       + "\n device id: " + SystemInfo.deviceUniqueIdentifier
                       + " \n App version: " + Application.version; 
            
            var mail = new Mail
            {
                Email = InputEmail.text,
                Subject = InputSubject.text,
                Body = body
            };
            _mailService.Open(mail);
        }
        
    }
}